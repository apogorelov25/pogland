import Input from './Input';
import PhoneInput from './Input';
import SharedSelect from './SharedSelect';

export default Input;
export { PhoneInput, SharedSelect };
